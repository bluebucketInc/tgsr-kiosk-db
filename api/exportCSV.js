// import plugins
import express from "express";
import path from "path";
import moment from "moment";

// router
export const exportCSV = express.Router();

// generator endpoint
exportCSV.get("/export", (req, res) => {
	// file name
	const fileName = `${moment().format("MM-DD_h:mm:ss_a")}.csv`;
	// event spawn
	const spawn = require("child_process").spawn,
		ls = spawn("mongoexport", [
			"--db",
			"tgsr_kiosk",
			"--collection",
			"tgsr_kiosk",
			"--out",
			`exports/${fileName}`
		]);
	// send response
	ls.stdout.on("close", (data) => {
		setTimeout(() => res.sendFile(path.join(__dirname, '..', `exports/${fileName}`), fileName), 1000);
	});
});
