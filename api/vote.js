// import plugins
import express from "express";

// utils
import { submitVote } from "../db";

// router
export const voteRoutes = express.Router();

// generator endpoint
voteRoutes.post("/vote", (req, res) => {
	console.log(req);
	// validate token
	submitVote(req.body.mashup)
		.then(() => {
			res.status(201).send({
				success: true
			});
		})
		.catch(err => {
			console.log(err);
			res.status(500).send({
				success: true
			});
		});
});
