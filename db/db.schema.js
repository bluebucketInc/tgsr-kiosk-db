
// lib imports
import mongoose from "mongoose";

// Form Data Schema
let tgsr_kiosk = new mongoose.Schema({
	mashup: String,
	date: String,
});

// export
module.exports = mongoose.model("tgsr_kiosk", tgsr_kiosk, "tgsr_kiosk");