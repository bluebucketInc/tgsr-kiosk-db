
// import plugins
import express from "express";
import moment from "moment";
import mongoose from "mongoose";

// settings
import { DB_URL, DB_USER, DB_PASSWORD } from "../utils/config";

// schema
import tgsr_kiosk from '../db/db.schema';

// connect to db
mongoose.connect( DB_URL, {
		useNewUrlParser: true,
		user: DB_USER,
		pass: DB_PASSWORD,
	})
	.then(db => console.log("connection successful"))
	.catch(e => console.log("connection error", e));



// Add form to DB
export const submitVote = payload =>
	new Promise((resolve, reject) => {
		// form instance
		let item = new tgsr_kiosk({
			mashup: payload,
			date: moment().format('MMMM Do YYYY, h:mm:ss a'),
		});
		// send
		item.save()
			.then(() => resolve())
			.catch(err => reject(err));
	});