// PM2 server config
module.exports = {
    apps: [
        {
            name: "lpg-form",
            script: "npm",
            cwd: "/var/www/server", // add location here
            args: "run production",
        }
    ]
};
