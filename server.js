// import plugins
import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";
import cors from "cors"

// settings
import { API_PORT } from "./utils/config";

// API routes
import { voteRoutes } from "./api/vote";
import { exportCSV } from "./api/exportCSV";

// init server
const server = express();

// add middleware
server.use(bodyParser.json());
server.use(cors());
server.use(bodyParser.urlencoded({ extended: true }));

// security
server.use(helmet());

// end points
server.use("/api", voteRoutes);
server.use("/db", exportCSV);

// broadcast
server.listen(API_PORT, () => console.log(`server running on port ${API_PORT}`));